import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from '../app/test/test.component';

const routes: Routes = [
  { path: '', redirectTo: 'test', pathMatch: 'full' },
  { path: 'test', component: TestComponent },
  // { path: 'component', canActivate: [AuthGuard], component: ComponentsComponent },
  // { path: 'user-profile', canActivate: [AuthGuard], component: ProfileComponent },
  // { path: 'signup', canActivate: [AuthGuard], component: SignupComponent },
  // { path: 'landing', canActivate: [AuthGuard], component: LandingComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
